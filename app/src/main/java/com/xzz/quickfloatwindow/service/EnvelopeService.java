package com.xzz.quickfloatwindow.service;

import android.accessibilityservice.AccessibilityService;
import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Context;
import android.os.Handler;
import android.text.TextUtils;
import android.view.accessibility.AccessibilityEvent;
import android.widget.Toast;

import com.xzz.quickfloatwindow.R;
import com.xzz.quickfloatwindow.observer.ScreenObserver;
import com.xzz.quickfloatwindow.receiver.AdminManageReceiver;

/**
 * 通过辅助功能实现返回功能
 *
 * @author xiao
 */
public class EnvelopeService extends AccessibilityService {

    private static EnvelopeService instance;
    private String className = "";
    private ScreenObserver mScreenObserver;
    private Handler handler = new Handler();

    @Override
    public void onAccessibilityEvent(AccessibilityEvent event) {
        int eventType = event.getEventType();
        if (AccessibilityEvent.TYPE_WINDOW_STATE_CHANGED == eventType) {
            className = event.getClassName().toString();
        }
    }

    @Override
    public void onInterrupt() {
        Toast.makeText(this, R.string.interrupt_service, Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onServiceConnected() {
        super.onServiceConnected();
        instance = this;
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {

        }
        performGlobalAction(GLOBAL_ACTION_BACK);
        Toast.makeText(this, R.string.link_service, Toast.LENGTH_SHORT).show();

        mScreenObserver = new ScreenObserver(this);
        mScreenObserver.requestScreenStateUpdate(new ScreenObserver.ScreenStateListener() {
            @Override
            public void onScreenOn() {
                handler.postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        className = "";
                    }
                }, 500);


                handler.postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        if (!TextUtils.isEmpty(className)) {
                            return;
                        }
                        //锁屏
                        DevicePolicyManager mDevicePolicyManager = (DevicePolicyManager) getSystemService(Context.DEVICE_POLICY_SERVICE);
                        if (mDevicePolicyManager.isAdminActive(new ComponentName(instance, AdminManageReceiver.class))) {
                            mDevicePolicyManager.lockNow();
                        }
                    }
                }, 8000);
            }

            @Override
            public void onScreenOff() {
            }
        });

    }

    public static EnvelopeService getInstance() {
        return instance;
    }

    public static void back() {
        if (instance == null) {
            return;
        }
        instance.performGlobalAction(GLOBAL_ACTION_BACK);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        instance = null;
        mScreenObserver.stopScreenStateUpdate();
    }

}